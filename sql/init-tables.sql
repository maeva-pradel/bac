CREATE TABLE IF NOT EXISTS `candidat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) NOT NULL,
  `prenom` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `matiere` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(15) NOT NULL,
  `libelle` varchar(50) NOT NULL,
  `coefficient` int(5) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE IF NOT EXISTS `resultat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `note` float NOT NULL,
  `id_matiere` int(11) NOT NULL,
  `id_candidat` int(11) NOT NULL,
  PRIMARY KEY (`id`)
);
ALTER TABLE `resultat` ADD FOREIGN KEY `fk_resultat_matiere` (`id_matiere`) REFERENCES `matiere` (`id`);
ALTER TABLE `resultat` ADD FOREIGN KEY `fk_resultat_candidat` (`id_candidat`) REFERENCES `candidat` (`id`);