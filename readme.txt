1. Créer le modèle de données	OK

	candidat
		id				PRIMARY KEY - INT AUTO INCREMENT
		prenom			VARCHAR(60)
		nom				VARCHAR(60)
	
	matiere
		id				PRIMARY KEY - INT AUTO INCREMENT
		code			VARCHAR(10)
		libelle			VARCHAR(60)
		coefficient		INT
	
	resultat
		id				PRIMARY KEY - INT AUTO INCREMENT
		note			DOUBLE
		id_candidat		FOREIGN KEY (vers candidat) - INT
		id_matiere		FOREIGN KEY (vers matiere) - INT

2. Scripts MySQL de création des tables

3. Créer les classes d'entité correspondant aux tables
	Ajout de la dépendance Spring Boot
	Utilisation de JPA / Hibernate
	
4. Création du controller EleveController
	Listing des candidats
		GET	/candidat
	Recherche de candidats
		GET	/candidat?nom=...&admin=...
	Récupération de candidat
		GET	/candidat/{id}
		
	2 services REST :
		GET /candidat
		GET /candidat/{id}
		
5. Développement de la partie métier
	Requêtage à la base de données
	Calcul de ce qui est nécessaire (moyennes, etc...)
	Construction du DTO (CandidatDto)
		moyenne
		admis ou non
		mention

	Controller -> Service -> Repository / Entités / Base de données
