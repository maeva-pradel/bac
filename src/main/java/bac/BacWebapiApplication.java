package bac;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@SpringBootApplication(scanBasePackages = { "bac" })
@EnableAutoConfiguration
@EnableAsync
public class BacWebapiApplication {

	public static final long serialVersionUID = 1L;

	private static ConfigurableApplicationContext ctx;

	public static void main(String[] args) {
		SpringApplication springApplication = new SpringApplication(BacWebapiApplication.class);
		ctx = springApplication.run(args);
		ctx.start();
	}

	@Bean
	public TaskScheduler taskScheduler() {
		final ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();
		scheduler.setPoolSize(10);
		return scheduler;
	}

	public static ConfigurableApplicationContext getCtx() {
		return ctx;
	}
}
